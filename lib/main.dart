import 'dart:ffi';

import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with TickerProviderStateMixin {
  AnimationController _buttonController;
  Animation<double> _buttonAnimation;

  AnimationController _topController;
  Animation<double> _topAnimation;

  AnimationController _colorController;
  Animation<Color> _colorAnimation;

  AnimationController _opacityController;
  Animation<double> _opacityAnimation;


  @override
  void initState() {
    _buttonController = AnimationController(
        vsync: this,
        duration: Duration(milliseconds: 1000),
        reverseDuration: Duration(milliseconds: 100));
    _buttonAnimation = Tween<double>(begin: 50, end: 150).animate(_buttonController);

    _topController = AnimationController(
        vsync: this,
        duration: Duration(milliseconds: 1000),
        reverseDuration: Duration(milliseconds: 100),);
    _topAnimation = Tween<double>(begin: 20, end: 200).animate(_topController);

    _opacityController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 4000),
      reverseDuration: Duration(milliseconds: 100),);
    _opacityAnimation = Tween<double>(begin: 0.0, end: 1.0).animate(_opacityController);

    _colorController = AnimationController(vsync: this, duration: Duration(milliseconds: 1000), reverseDuration: Duration(milliseconds: 200));

    _colorAnimation = ColorTween(begin: Colors.green, end: Colors.orange,).animate(_colorController);


    _colorController.addListener((update));
    _buttonController.addListener(update);
    _topController.addListener(update);
    _opacityController.addListener(update);

    super.initState();
  }

  @override
  void dispose() {

    _opacityController.dispose();
    _topController.dispose();
    _buttonController.dispose();
    _colorController.dispose();
    super.dispose();
  }

  void update() => setState((){});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            InkWell(
              onTap: (){
                print("tap");
                _buttonController.forward();
                _topController.forward();
                _colorController.forward();
                _opacityController.forward();
              },
              child: AnimatedContainer(
                width: double.infinity,
                height: _topAnimation.value,
                color: _colorAnimation.value,
                duration: Duration(seconds: 1),
                child: Center(
                  child: /*Animated*/Opacity(
                    opacity: _opacityAnimation.value,
// duration: Duration(seconds: 1),
                    child: Text(
                      'it\'s awesome',
                    ),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 150),
              child: Container(
                width: 150.0,
                height: _buttonAnimation.value,
                color: _colorAnimation.value,
                // duration: Duration(seconds: 1),
                child: InkWell(
                  onTap: (){
                    _buttonController.reset();
                    _colorController.reset();
                    _topController.reset();
                    _opacityController.reset();
                  },
                    child: Center(

                    child: Text(
                      'tap me',
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }


}
